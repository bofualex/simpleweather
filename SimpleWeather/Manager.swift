
import Foundation

//MARK: protocol

@objc protocol  ManagerDelegate: class {
    optional func doneGettingDetailedWeather(response: [Weather])
    optional func doneGettingWeeklyWeather(response: [WeeklyWeather])
    optional func doneGettingCity(city: String)
}

//MARK: class

class Manager: NSObject, ApiRequestDelegate, GeoCodeDelegate {
    
    let managerAPIday = ApiRequest()
    let managerAPIweek = ApiRequest()

    let managerGeo = GeoCode()
    
    var weatherResponse: NSDictionary!
    var location: String?
    
    weak var delegate: ManagerDelegate?
    
    //MARK: instance methods
    
    func allRequests(location: String) {
        managerGeo.delegate = self
        managerGeo.geoCode(location)
        managerAPIday.delegate = self
    }
    
    func requestDetailedWeather() {
        managerAPIday.delegate = self
        managerAPIday.requestDailyWeather(GeoCode().currentLocation())
    }
    
    func requestWeeklyWeather(address: String) {
        managerGeo.delegate = self
        managerGeo.geoCode(address)
        managerAPIweek.delegate = self
    }
    
    func requestCity() {
        managerGeo.delegate = self
        managerGeo.reverseGeoCode()
    }
    
    //MARK: delegate methods
    
    func detailDailyRequest(response: [WeeklyWeather]) {
        delegate?.doneGettingWeeklyWeather!(response)
    }
    
    func detailRequestDelegate(response: [Weather]) {
        delegate?.doneGettingDetailedWeather!(response)
    }
    
    func geodelegate(coordinates: String) {
        location = coordinates
        managerAPIday.requestDailyWeather(location!)
    }
    
    func reversegeo(city: String) {
        delegate?.doneGettingCity!(city)
    }

}


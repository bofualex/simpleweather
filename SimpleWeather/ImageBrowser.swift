

import UIKit

class ImageBrowser: UIViewController {

    var address: String?
    @IBOutlet var webView: UIWebView?
    
    //MARK: instance methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageSearch()
    }
    
    func imageSearch() {
        var urlString: String = address!
        if urlString.characters.last == " " {
            urlString = String(urlString.characters.dropLast())
        }
        urlString = urlString.stringByReplacingOccurrencesOfString(" ", withString: "+", options: NSStringCompareOptions.LiteralSearch, range: nil)
        let url = NSURL(string: "https://www.google.com/search?site=&tbm=isch&source=hp&biw=1264&bih=732&q=\(urlString)")
        let request = NSURLRequest(URL: url!)
        self.webView!.loadRequest(request)
    }
    
    //MARK: action methods
    
    @IBAction func back(sender: UIButton) {
        if let parentVC = self.parentViewController as? CustomTable {
        parentVC.dismissButton?.hidden = false
        }
        //self.webView?.goBack()
        self.willMoveToParentViewController(nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }

}

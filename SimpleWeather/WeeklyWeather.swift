
import Foundation

class WeeklyWeather: NSObject {
    
    
    let apparentTemperatureMax: NSNumber
    let apparentTemperatureMin: NSNumber
    let cloudCover: NSNumber
    let humidity: NSNumber
    let icon: String
    let summary: String
    let windSpeed: NSNumber
    let time: NSNumber
    let sunsetTime: NSNumber
    let sunriseTime: NSNumber
    let temperatureMaxTime: NSNumber
    let temperatureMinTime: NSNumber
    let precipProbability: NSNumber
    
    init(apparentTemperatureMax: NSNumber,apparentTemperatureMin: NSNumber, cloudCover: NSNumber, humidity: NSNumber, icon: String, summary: String, windSpeed: NSNumber, time: NSNumber, sunsetTime: NSNumber, sunriseTime: NSNumber, temperatureMaxTime: NSNumber, temperatureMinTime: NSNumber, precipProbability: NSNumber){
        self.apparentTemperatureMax = apparentTemperatureMax
        self.apparentTemperatureMin = apparentTemperatureMin
        self.cloudCover = cloudCover
        self.humidity = humidity
        self.icon = icon
        self.summary = summary
        self.windSpeed = windSpeed
        self.time = time
        self.sunsetTime = sunsetTime
        self.sunriseTime = sunriseTime
        self.temperatureMaxTime = temperatureMaxTime
        self.temperatureMinTime = temperatureMinTime
        self.precipProbability = precipProbability
    }
    
    init(dictionary: NSDictionary) {
        apparentTemperatureMax = dictionary["apparentTemperatureMax"] as! NSNumber
        apparentTemperatureMin = dictionary["apparentTemperatureMin"] as! NSNumber
        cloudCover = dictionary["cloudCover"] as! NSNumber
        humidity = dictionary["humidity"] as! NSNumber
        icon = dictionary["icon"] as! String
        summary = dictionary["summary"] as! String
        windSpeed = dictionary["windSpeed"] as! NSNumber
        time = dictionary["time"] as! NSNumber
        sunriseTime = dictionary["sunriseTime"] as! NSNumber
        sunsetTime = dictionary["sunsetTime"] as! NSNumber
        temperatureMaxTime = dictionary["temperatureMaxTime"] as! NSNumber
        temperatureMinTime = dictionary["temperatureMinTime"] as! NSNumber
        precipProbability = dictionary["precipProbability"] as! NSNumber
    }
    
}
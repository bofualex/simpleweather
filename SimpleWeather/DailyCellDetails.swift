
import UIKit

class CustomCell: UITableViewCell {
    
    @IBOutlet var day1: UILabel?
    @IBOutlet var day1TempView: UIImageView?
    @IBOutlet var day1MinTempLabel: UILabel?
    @IBOutlet var day1MaxTempLabel: UILabel?
    @IBOutlet var day1sunriseLabel: UILabel?
    @IBOutlet var day1sunsetLabel: UILabel?
    @IBOutlet var day1windSpeedLabel: UILabel?
    @IBOutlet var day1humidityLabel: UILabel?
    @IBOutlet var day1summaryLabel: UILabel?
    @IBOutlet var background: UIImageView?

    override func awakeFromNib() {
        super.awakeFromNib()
        viewDidAppear(true)
    }

    func viewDidAppear(animated: Bool) {
        let translate = CGAffineTransformMakeTranslation(0, 500)
        day1TempView?.transform = translate
        day1sunriseLabel?.transform = translate
        day1sunsetLabel?.transform = translate
        day1humidityLabel?.transform = translate
        day1windSpeedLabel?.transform = translate
        
        UIView.animateWithDuration(2.5, delay: 0.2, usingSpringWithDamping: 0.8, initialSpringVelocity: 1, options: [], animations: {
            self.day1TempView?.transform = CGAffineTransformIdentity
            self.day1sunriseLabel?.transform = CGAffineTransformIdentity
            self.day1sunsetLabel?.transform = CGAffineTransformIdentity
            self.day1humidityLabel?.transform = CGAffineTransformIdentity
            self.day1windSpeedLabel?.transform = CGAffineTransformIdentity
            }, completion: nil)
    }
}

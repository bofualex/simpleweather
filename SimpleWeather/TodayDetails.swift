

import Foundation
import UIKit

class AddChild: UIViewController {
    
    var weatherResponse: [Weather]?
    var address: String?
    static let sharedInstance = AddChild()
    
    @IBOutlet var dailyTemp1View: UIImageView?
    @IBOutlet var dailyTemp2View: UIImageView?
    @IBOutlet var dailyTemp3View: UIImageView?
    @IBOutlet var currentTempView: UIImageView?
    @IBOutlet var backgroundImage: UIImageView?
    
    @IBOutlet var dailyTemp1label: UILabel?
    @IBOutlet var dailyTemp2label: UILabel?
    @IBOutlet var dailyTemp3label: UILabel?
    @IBOutlet var currentTempLabel: UILabel?
    @IBOutlet var windSpeed: UILabel?
    
    @IBOutlet var dailyClock1: UILabel?
    @IBOutlet var dailyClock2: UILabel?
    @IBOutlet var dailyClock3: UILabel?
    @IBOutlet var currentClock: UILabel?
    @IBOutlet var locationLabel: UILabel?
    
    //MARK: instance methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
        animate()
    }
    
    override func viewDidAppear(animated: Bool) {
        let translate = CGAffineTransformMakeTranslation(0, 500)
        currentTempView!.transform = translate
        currentClock!.transform = translate
        currentTempLabel!.transform = translate
        locationLabel!.transform = translate
        
        dailyTemp1View!.transform = translate
        dailyTemp2View!.transform = translate
        dailyTemp3View!.transform = translate
        
        dailyClock1!.transform = translate
        dailyClock2!.transform = translate
        dailyClock3!.transform = translate
        
        dailyTemp1label!.transform = translate
        dailyTemp2label!.transform = translate
        dailyTemp3label!.transform = translate
        
        UIView.animateWithDuration(2.5, delay: 0.2, usingSpringWithDamping: 0.8, initialSpringVelocity: 1, options: [UIViewAnimationOptions.Autoreverse, UIViewAnimationOptions.Repeat, UIViewAnimationOptions.CurveEaseInOut], animations: {
            self.currentTempView!.transform = CGAffineTransformIdentity
            self.dailyTemp1View!.transform = CGAffineTransformIdentity
            self.dailyTemp2View!.transform = CGAffineTransformIdentity
            self.dailyTemp3View!.transform = CGAffineTransformIdentity
            }, completion: nil)
        UIView.animateWithDuration(2.5, delay: 0.4, usingSpringWithDamping: 0.8, initialSpringVelocity: 1, options: [.Repeat, .Autoreverse, .ShowHideTransitionViews], animations: {
            self.currentClock!.transform = CGAffineTransformIdentity
            self.currentTempLabel!.transform = CGAffineTransformIdentity
            self.locationLabel!.transform = CGAffineTransformIdentity
            self.dailyClock1!.transform = CGAffineTransformIdentity
            self.dailyClock2!.transform = CGAffineTransformIdentity
            self.dailyClock3!.transform = CGAffineTransformIdentity
            self.dailyTemp1label!.transform = CGAffineTransformIdentity
            self.dailyTemp2label!.transform = CGAffineTransformIdentity
            self.dailyTemp3label!.transform = CGAffineTransformIdentity
            self.dailyTemp1label!.transform = CGAffineTransformIdentity
            self.dailyTemp2label!.transform = CGAffineTransformIdentity
            self.dailyTemp3label!.transform = CGAffineTransformIdentity
            }, completion: nil)
    }
    
    func updateUI() {
        
        dailyClock1?.text = WeatherInterpretation.formatTime(weatherResponse![3].time)
        dailyClock2?.text = WeatherInterpretation.formatTime(weatherResponse![7].time)
        dailyClock3?.text = WeatherInterpretation.formatTime(weatherResponse![11].time)
        currentClock?.text = WeatherInterpretation.formatTime(weatherResponse![0].time)
        locationLabel?.text? = address!
        locationLabel?.text?.capitalizedString
        
        WeatherInterpretation.changeBackground(weatherResponse![0], backgroundImage: backgroundImage!, weatherImage: currentTempView!, tempLabel: currentTempLabel!, windSpeed: windSpeed!)
        WeatherInterpretation.changeIcon(weatherResponse![3], weatherImage: dailyTemp1View!, tempLabel: dailyTemp1label!)
        WeatherInterpretation.changeIcon(weatherResponse![7], weatherImage: dailyTemp2View!, tempLabel: dailyTemp2label!)
        WeatherInterpretation.changeIcon(weatherResponse![11], weatherImage: dailyTemp3View!, tempLabel: dailyTemp3label!)
    }
    
    func animate() {
        let path = UIBezierPath()
        path.moveToPoint(CGPoint(x: 16,y: 239))
        path.addCurveToPoint(CGPoint(x: 301, y: 239), controlPoint1: CGPoint(x: 136, y: 373), controlPoint2: CGPoint(x: 178, y: 110))
        
        let anim = CAKeyframeAnimation(keyPath: "position")
        
        anim.path = path.CGPath
        
        anim.rotationMode = kCAAnimationRotateAuto
        anim.repeatCount = Float.infinity
        anim.duration = 10.0
        
        currentTempView!.layer.addAnimation(anim, forKey: "animate position along path")
    }

    //MARK: action methods
    
    @IBAction func back(sender: UIButton) {
        self.willMoveToParentViewController(nil)
        parentViewController!.navigationController?.navigationBarHidden = false
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    
}


import Foundation
import CoreLocation

//MARK: protocol

@objc protocol GeoCodeDelegate: class {
    optional func geodelegate(coordinates: String)
    optional func reversegeo(city: String)
}

//MARK: class

class GeoCode: NSObject, CLLocationManagerDelegate {
    
    weak var delegate: GeoCodeDelegate?
    let geocoder = CLGeocoder()
    var locationManager = CLLocationManager()
    
    var address: String?
    var currentLoc: String?
    
    //MARK: instance methods
    
    func geoCode(location: String) {
        geocoder.geocodeAddressString(location, completionHandler: {(placemarks, error) -> Void in
            if(error != nil) {
                print(error?.localizedDescription)
                //("This is a mock up location \n Please enter valid location")
            }
            if let placemark = placemarks?.first {
                let coordinates:CLLocationCoordinate2D = placemark.location!.coordinate
                let string = "\(coordinates.latitude)" + "," + "\(coordinates.longitude)"
                self.delegate?.geodelegate!(string)
            }
        })
    }
    
    func reverseGeoCode() {
        geocoder.reverseGeocodeLocation(self.locationManager.location!, completionHandler: {
            (placemarks, error) -> Void in
            if (error != nil) {
                print(error!.localizedDescription)
                return
            }
            if placemarks!.count>0 {
                let pm = placemarks!.last! as CLPlacemark
                let locality = pm.locality
                self.delegate?.reversegeo!(locality!)
            }
        })
    }
    
    func updateLocation() {
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startMonitoringSignificantLocationChanges()
        self.locationManager.startUpdatingLocation()
        currentLoc = "\(self.locationManager.location!.coordinate.latitude)" + "," + "\(self.locationManager.location!.coordinate.longitude)"
    }
    
    func currentLocation() -> String {
        updateLocation()
        return currentLoc!
    }
    
    func mapLocation() -> CLLocation{
        updateLocation()
        return self.locationManager.location!
    }
    //MARK: delegate methods
    
    func doneGettingLocation(location: String) {
        address = location
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last! as CLLocation
        self.currentLoc = "\(location.coordinate.latitude)" + "," + "\(location.coordinate.longitude)"
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print(error.localizedDescription)
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        switch status {
        case .NotDetermined:
            self.currentLoc = "23,46"
            print(".NotDetermined")
            break
            
        case .AuthorizedAlways:
            print(".AuthorizedAlways")
            break
            
        case .Denied:
            print(".Denied")
            self.currentLoc = "23,46"
            break
            
        case .AuthorizedWhenInUse:
            print(".AuthorizedWhenInUse")
            break
            
        case .Restricted:
            self.currentLoc = "23,46"
            break
        }
    }
}

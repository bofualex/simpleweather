

import Foundation
import UIKit

class WeatherInterpretation: NSObject {
    
    //MARK: class methods
    
    class func changeBackground(weather: Weather, backgroundImage: UIImageView, weatherImage: UIImageView, tempLabel: UILabel, windSpeed: UILabel) {
        if weather.icon == "clear-day" || weather.icon == "wind" {
            backgroundImage.image = UIImage(named: "sunny")
            weatherImage.image = UIImage(named: "sun.png")
        } else if weather.icon == "rain" || weather.icon == "sleet"{
            backgroundImage.image = UIImage(named: "rainy")
            weatherImage.image = UIImage(named: "sun_rain.png")
        } else if weather.icon == "fog" || weather.icon == "cloudy" {
            backgroundImage.image = UIImage(named: "cloudy")
            weatherImage.image = UIImage(named: "cloud_sun.png")
        } else if weather.icon == "snow" {
            backgroundImage.image = UIImage(named: "snowy")
            weatherImage.image = UIImage(named: "sun_snow.png")
        } else if weather.icon == "partly-cloudy-day" {
            backgroundImage.image = UIImage(named: "sunny")
            weatherImage.image = UIImage(named: "cloud_sun.png")
        } else if weather.icon == "clear-night" || weather.icon == "partly-cloudy-night" {
            backgroundImage.image = UIImage(named: "night")
            weatherImage.image = UIImage(named: "night_cloud")
        }
        tempLabel.text! = String(Int(weather.apparentTemperature)) + " ˚C"
        windSpeed.text! = String(Int(Double(weather.windSpeed)*10)) + " km/h"
    }
    
    class func changeBackgroundWeekly(weather: WeeklyWeather, backgroundImage: UIImageView, day: UILabel, weatherImage: UIImageView, tempMaxLabel: UILabel, tempMinLabel: UILabel, sunrise: UILabel, sunset: UILabel, windSpeed: UILabel, humidity: UILabel, summary: UILabel) {
        if weather.icon == "clear-day" || weather.icon == "wind" {
            weatherImage.image = UIImage(named: "sun.png")
            backgroundImage.image = UIImage(named: "sunny")
        } else if weather.icon == "rain" || weather.icon == "sleet"{
            backgroundImage.image = UIImage(named: "rainy")
            weatherImage.image = UIImage(named: "sun_rain.png")
        } else if weather.icon == "fog" || weather.icon == "cloudy" {
            backgroundImage.image = UIImage(named: "cloudy")
            weatherImage.image = UIImage(named: "cloud_sun.png")
        } else if weather.icon == "snow" {
            backgroundImage.image = UIImage(named: "snowy")
            weatherImage.image = UIImage(named: "sun_snow.png")
        } else if weather.icon == "partly-cloudy-day" {
            backgroundImage.image = UIImage(named: "sunny")
            weatherImage.image = UIImage(named: "cloud_sun.png")
        } else if weather.icon == "clear-night" || weather.icon == "partly-cloudy-night" {
            backgroundImage.image = UIImage(named: "night")
            weatherImage.image = UIImage(named: "night_cloud")
        }
        day.text! = formatDate(weather.time)
        tempMaxLabel.text! = String(Int(weather.apparentTemperatureMax)) + " ˚C"
        tempMinLabel.text! = String(Int(weather.apparentTemperatureMin)) + " ˚C"
        sunrise.text! = formatTime(weather.sunriseTime)
        sunset.text! = formatTime(weather.sunsetTime)
        windSpeed.text! = String(Int(Double(weather.windSpeed)*10)) + " km/h"
        humidity.text! = String(Int(Double(weather.humidity)*100)) + " %"
        summary.text! = weather.summary
    }
    
    class func changeWeeklyFeatures(weather: WeeklyWeather,day: UILabel, weatherImage: UIImageView, tempMaxLabel: UILabel, tempMinLabel: UILabel, sunrise: UILabel, sunset: UILabel, windSpeed: UILabel, humidity: UILabel, summary: UILabel) {
        if weather.icon == "clear-day" || weather.icon == "wind" {
            weatherImage.image = UIImage(named: "sun.png")
        } else if weather.icon == "rain" || weather.icon == "sleet"{
            weatherImage.image = UIImage(named: "sun_rain.png")
        } else if weather.icon == "fog" || weather.icon == "cloudy" {
            weatherImage.image = UIImage(named: "cloud_sun.png")
        } else if weather.icon == "snow" {
            weatherImage.image = UIImage(named: "sun_snow.png")
        } else if weather.icon == "partly-cloudy-day" {
            weatherImage.image = UIImage(named: "cloud_sun.png")
        } else if weather.icon == "clear-night" || weather.icon == "partly-cloudy-night" {
            weatherImage.image = UIImage(named: "night_cloud")
        }
        day.text! = formatDate(weather.time)
        tempMaxLabel.text! = String(Int(weather.apparentTemperatureMax)+2) + " ˚C"
        tempMinLabel.text! = String(Int(weather.apparentTemperatureMin)) + " ˚C"
        sunrise.text! = formatTime(weather.sunriseTime)
        sunset.text! = formatTime(weather.sunsetTime)
        windSpeed.text! = String(Int(Double(weather.windSpeed)*10)) + " km/h"
        humidity.text! = String(Int(Double(weather.humidity)*100)) + " %"
        summary.text! = weather.summary
    }
    
    class func changeIcon(weather: Weather,  weatherImage: UIImageView, tempLabel: UILabel) {
        if weather.icon == "clear-day" || weather.icon == "wind" {
            weatherImage.image = UIImage(named: "sun.png")
        } else if weather.icon == "rain" || weather.icon == "sleet"{
            weatherImage.image = UIImage(named: "sun_rain.png")
        } else if weather.icon == "fog" || weather.icon == "cloudy" {
            weatherImage.image = UIImage(named: "cloud_sun.png")
        } else if weather.icon == "snow" {
            weatherImage.image = UIImage(named: "sun_snow.png")
        } else if weather.icon == "partly-cloudy-day" {
            weatherImage.image = UIImage(named: "cloud_sun.png")
        } else if weather.icon == "clear-night" || weather.icon == "partly-cloudy-night" {
            weatherImage.image = UIImage(named: "night_cloud")
        }
        tempLabel.text! = String(Int(weather.apparentTemperature)) + " ˚C"
    }
    
    
    class func formatTime(weatherRe: NSNumber) -> String {
        let date = NSDate(timeIntervalSince1970: Double(weatherRe))
        let timeFormatter = NSDateFormatter()
        timeFormatter.locale = NSLocale.currentLocale()
        timeFormatter.timeStyle = .ShortStyle
        let convertedTime = timeFormatter.stringFromDate(date)
        return convertedTime
    }
    
    class func formatDate(weatherRe: NSNumber) -> String {
        let date = NSDate(timeIntervalSince1970: Double(weatherRe))
        let timeFormatter = NSDateFormatter()
        timeFormatter.locale = NSLocale.currentLocale()
        timeFormatter.dateStyle = .MediumStyle
        let convertedTime = timeFormatter.stringFromDate(date)
        return convertedTime
    }
    
    class func formatDateShort(weatherRe: NSNumber) -> String {
        let date = NSDate(timeIntervalSince1970: Double(weatherRe))
        let timeFormatter = NSDateFormatter()
        timeFormatter.locale = NSLocale.currentLocale()
        timeFormatter.dateStyle = .ShortStyle
        let convertedTime = timeFormatter.stringFromDate(date)
        return convertedTime
    }
    
}

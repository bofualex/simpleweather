

import Foundation
import UIKit

class DailyDetails: UIViewController {
    
    var weatherResponse: [WeeklyWeather]?
    var address: String?
    
    @IBOutlet var day1: UILabel?
    @IBOutlet var day1TempView: UIImageView?
    @IBOutlet var day1MinTempLabel: UILabel?
    @IBOutlet var day1MaxTempLabel: UILabel?
    @IBOutlet var day1sunriseLabel: UILabel?
    @IBOutlet var day1sunsetLabel: UILabel?
    @IBOutlet var day1windSpeedLabel: UILabel?
    @IBOutlet var day1humidityLabel: UILabel?
    @IBOutlet var day1summaryLabel: UILabel?
    
    @IBOutlet var day2: UILabel?
    @IBOutlet var day2TempView: UIImageView?
    @IBOutlet var day2MinTempLabel: UILabel?
    @IBOutlet var day2MaxTempLabel: UILabel?
    @IBOutlet var day2sunriseLabel: UILabel?
    @IBOutlet var day2sunsetLabel: UILabel?
    @IBOutlet var day2windSpeedLabel: UILabel?
    @IBOutlet var day2humidityLabel: UILabel?
    @IBOutlet var day2summaryLabel: UILabel?
    
    @IBOutlet var day3: UILabel?
    @IBOutlet var day3TempView: UIImageView?
    @IBOutlet var day3MinTempLabel: UILabel?
    @IBOutlet var day3MaxTempLabel: UILabel?
    @IBOutlet var day3sunriseLabel: UILabel?
    @IBOutlet var day3sunsetLabel: UILabel?
    @IBOutlet var day3windSpeedLabel: UILabel?
    @IBOutlet var day3humidityLabel: UILabel?
    @IBOutlet var day3summaryLabel: UILabel?
    
    @IBOutlet var backgroundImage: UIImageView?
    
    //MARK: instance methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }
    
    override func viewDidAppear(animated: Bool) {
        let translate = CGAffineTransformMakeTranslation(100, 400)
        day1TempView?.transform = translate
        day2TempView?.transform = translate
        day3TempView?.transform = translate
        
        UIView.animateWithDuration(2.5, delay: 0.2, usingSpringWithDamping: 0.8, initialSpringVelocity: 1, options: [.TransitionCrossDissolve], animations: {
            self.day1TempView?.transform = CGAffineTransformIdentity
            self.day2TempView?.transform = CGAffineTransformIdentity
            self.day3TempView?.transform = CGAffineTransformIdentity
            }, completion: nil)
    }
        
    func updateUI() {
        WeatherInterpretation.changeWeeklyFeatures(weatherResponse![1], day: day1!, weatherImage: day1TempView!, tempMaxLabel: day1MaxTempLabel!, tempMinLabel: day1MinTempLabel!, sunrise: day1sunriseLabel!, sunset: day1sunsetLabel!, windSpeed: day1windSpeedLabel!, humidity: day1humidityLabel!, summary: day1summaryLabel!)
        WeatherInterpretation.changeWeeklyFeatures(weatherResponse![2], day: day2!, weatherImage: day2TempView!, tempMaxLabel: day2MaxTempLabel!, tempMinLabel: day2MinTempLabel!, sunrise: day2sunriseLabel!, sunset: day2sunsetLabel!, windSpeed: day2windSpeedLabel!, humidity: day2humidityLabel!, summary: day2summaryLabel!)
        WeatherInterpretation.changeWeeklyFeatures(weatherResponse![3], day: day3!, weatherImage: day3TempView!, tempMaxLabel: day3MaxTempLabel!, tempMinLabel: day3MinTempLabel!, sunrise: day3sunriseLabel!, sunset: day3sunsetLabel!, windSpeed: day3windSpeedLabel!, humidity: day3humidityLabel!, summary: day3summaryLabel!)
    }

    //MARK: action methods
    
    @IBAction func back(sender: UIButton) {
        self.willMoveToParentViewController(nil)
        parentViewController!.navigationController?.navigationBarHidden = false
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    
}

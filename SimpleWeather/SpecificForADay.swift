

import Foundation
import UIKit

class SpecificforADay: UIViewController {
    
    var address: String?
    var weather: WeeklyWeather?
    
    @IBOutlet var dailyTempView: UIImageView?
    @IBOutlet var backgroundImage: UIImageView?
    
    @IBOutlet var day: UILabel?
    @IBOutlet var maxTemp: UILabel?
    @IBOutlet var maxHour: UILabel?
    @IBOutlet var minTemp: UILabel?
    @IBOutlet var minHour: UILabel?
    
    @IBOutlet var humidity: UILabel?
    @IBOutlet var windspeed: UILabel?
    @IBOutlet var sunrise: UILabel?
    @IBOutlet var sunset: UILabel?
    @IBOutlet var precipProb: UILabel?
    @IBOutlet var summary: UILabel?

    
    //MARK: instance methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }
    
    func updateUI() {
        WeatherInterpretation.changeBackgroundWeekly(weather!, backgroundImage: backgroundImage!, day: day!, weatherImage: dailyTempView!, tempMaxLabel: maxTemp!, tempMinLabel: minTemp!, sunrise: sunrise!, sunset: sunset!, windSpeed: windspeed!, humidity: humidity!, summary: summary!)
        maxHour?.text = WeatherInterpretation.formatTime(weather!.temperatureMaxTime)
        minHour?.text = WeatherInterpretation.formatTime(weather!.temperatureMinTime)
        precipProb?.text = String(Int(Double(weather!.precipProbability) * 100)) + " %"
    }
    
    //MARK: action methods
    
    @IBAction func back(sender: UIButton) {
        if let parentVC = self.parentViewController as? CustomTable {
            parentVC.dismissButton?.hidden = false
        }
        self.willMoveToParentViewController(nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }

}
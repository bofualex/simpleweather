

import UIKit
import Foundation

class CustomTable: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var weatherResponse: [WeeklyWeather]?
    var address: String?
    var dismissButton: UIButton?
    @IBOutlet var tableView: UITableView?
    
    //MARK: instance methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        doneButton()
        tableView!.delegate = self
        tableView!.dataSource = self
        self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func doneButton() {
        dismissButton = UIButton()
        dismissButton?.translatesAutoresizingMaskIntoConstraints = false
        dismissButton!.backgroundColor = UIColor.redColor()
        dismissButton!.layer.cornerRadius = 8
        dismissButton?.setTitle("Done", forState: .Normal)
        dismissButton?.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        dismissButton?.titleLabel?.font = UIFont.boldSystemFontOfSize(12.0)
        dismissButton?.addTarget(self, action: #selector(buttonClicked), forControlEvents: .TouchUpInside)
        self.view.addSubview(dismissButton!)
        
        let pinTop = NSLayoutConstraint(item: dismissButton!, attribute: .Top, relatedBy: .Equal, toItem: view, attribute: .Top, multiplier: 1.0, constant: 29)
        let pinLeft = NSLayoutConstraint(item: dismissButton!, attribute: .Leading, relatedBy: .Equal, toItem: view, attribute: .Leading, multiplier: 1.0, constant: 8)
        let width = NSLayoutConstraint(item: dismissButton!, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1.0, constant: 40)
        let height = NSLayoutConstraint(item: dismissButton!, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1.0, constant: 20)
        view.addConstraints([pinTop, pinLeft, width, height])
    }
    
    func buttonClicked(sender: UIButton) {
        self.willMoveToParentViewController(nil)
        parentViewController!.navigationController?.navigationBarHidden = false
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cellIdentifier = "Cell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! CustomCell
        WeatherInterpretation.changeBackgroundWeekly(weatherResponse![indexPath.row], backgroundImage: cell.background!, day: cell.day1!, weatherImage: cell.day1TempView!, tempMaxLabel: cell.day1MaxTempLabel!, tempMinLabel: cell.day1MinTempLabel!, sunrise: cell.day1sunriseLabel!, sunset: cell.day1sunsetLabel!, windSpeed: cell.day1windSpeedLabel!, humidity: cell.day1humidityLabel!, summary: cell.day1summaryLabel!)
        
        return cell
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        
        let moreAction = UITableViewRowAction(style: UITableViewRowActionStyle.Default, title: "More", handler: { (action, indexPath) -> Void in
            let addChildView = self.storyboard?.instantiateViewControllerWithIdentifier("Specific") as! SpecificforADay
            self.addChildViewController(addChildView)
            addChildView.address = self.address
            addChildView.weather = self.weatherResponse![indexPath.row]
            self.view.addSubview((addChildView.view)!)
            
            let viewsDict = ["child" : addChildView.view]
            self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[child]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDict))
            self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[child]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDict))
            addChildView.didMoveToParentViewController(self)
            self.dismissButton!.hidden = true
        })
        
        let imagesAction = UITableViewRowAction(style: UITableViewRowActionStyle.Default, title: "Images", handler: { (action, indexPath) -> Void in
            let addChildView = self.storyboard?.instantiateViewControllerWithIdentifier("ImageBrowser") as! ImageBrowser
            self.addChildViewController(addChildView)
            addChildView.address = self.address
            self.view.addSubview((addChildView.view)!)
            
            let viewsDict = ["child" : addChildView.view]
            self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[child]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDict))
            self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[child]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDict))
            addChildView.didMoveToParentViewController(self)
            self.dismissButton!.hidden = true
            
        })
        
        let backAction = UITableViewRowAction(style: UITableViewRowActionStyle.Default, title: "Back", handler: { (action, indexPath) -> Void in
            self.willMoveToParentViewController(nil)
            self.parentViewController!.navigationController?.navigationBarHidden = false
            self.view.removeFromSuperview()
            self.removeFromParentViewController()
        })
        moreAction.backgroundColor = UIColor.brownColor()
        imagesAction.backgroundColor = UIColor.darkGrayColor()
        backAction.backgroundColor = UIColor.redColor()
        return [backAction, imagesAction,moreAction]
    }
    
    //MARK: action methods
    
    func back(sender: UIButton) {
        self.willMoveToParentViewController(nil)
        parentViewController!.navigationController?.navigationBarHidden = false
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    
}


import Foundation
import UIKit
import Charts

class BarChart: UIViewController, ChartViewDelegate {
    
    var chartValues: [WeeklyWeather]!
    
    @IBOutlet weak var bubbleChartView: BubbleChartView!
    @IBOutlet var scroll: UIScrollView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scroll?.contentSize = CGSize(width: 630, height: 603)
        bubbleChartView.delegate = self
        loadCharts()
    }
    
    func loadCharts() {
        var array: [NSNumber] = []
        var arr: [String] = []
        var arr2: [Double] = []
        var arr3: [Double] = []

        for i in 0..<chartValues.count {
            array.append(chartValues[i].time)
        }
        for i in 0..<array.count {
            arr.append(WeatherInterpretation.formatDateShort(array[i]))
        }
        for i in 0..<chartValues.count {
            arr2.append(Double(chartValues[i].apparentTemperatureMax))
            arr3.append(Double(chartValues[i].apparentTemperatureMin))
        }
        GraphFunctions.setBubbleChart(arr, values: arr2, values2: arr3, barChartView: bubbleChartView)
    }
        
}

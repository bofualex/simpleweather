import Foundation
import Charts

class GraphFunctions: ChartViewDelegate {
    
    
    class func setBubbleChart(dataPoints: [String], values: [Double], values2: [Double], barChartView: BubbleChartView!) {
        
        barChartView.noDataText = "You need to provide data for the chart."
        
        var dataEntries: [BubbleChartDataEntry] = []
        
        for i in 0..<(dataPoints.count-1){
            let dataEntry = BubbleChartDataEntry(xIndex: i, value: values[i], size: CGFloat(values[i]))
            let dataEntry1 = BubbleChartDataEntry(xIndex: i, value: values2[i], size: CGFloat(values2[i]))
            dataEntries.append(dataEntry)
            dataEntries.append(dataEntry1)
        }
        
        let chartDataSet = BubbleChartDataSet(yVals: dataEntries, label: "Temps per day (Max Min)")
        let chartData = BubbleChartData(xVals: dataPoints, dataSet: chartDataSet)
        barChartView.data = chartData
        barChartView.descriptionText = ""
        chartDataSet.colors = ChartColorTemplates.colorful()
        
        barChartView.xAxis.labelPosition = .Bottom
        barChartView.xAxis.setLabelsToSkip(0)
        barChartView.bubbleData?.setValueFont(NSUIFont.boldSystemFontOfSize(14))
        barChartView.bubbleData?.setValueTextColor(NSUIColor.whiteColor())
        barChartView.bubbleData?.setDrawValues(true)
        
        barChartView.backgroundColor = UIColor(red: 189/255, green: 195/255, blue: 199/255, alpha: 1)
        barChartView.animate(xAxisDuration: 2.0, yAxisDuration: 3.0, easingOption: .EaseInBounce)
    }

}

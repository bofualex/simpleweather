
import Foundation
import UIKit

//MARK: class

class MainWeatherScreen: UIViewController, ManagerDelegate, UITextFieldDelegate {
    
    var weatherResponse: [Weather]!
    var weeklyWeather: [WeeklyWeather]!
    let manager = Manager()
    
    @IBOutlet var backgroundImage: UIImageView!
    @IBOutlet var weatherImage: UIImageView!
    @IBOutlet var tempLabel: UILabel!
    @IBOutlet var windSpeed: UILabel!
    @IBOutlet var currentTime: UILabel?
    @IBOutlet var cityLabel: UILabel?
    @IBOutlet var humidityLabel: UILabel?
    @IBOutlet var summaryLabel: UILabel?
    
    var dismissButton: UIButton?
    var searchTextField: UITextField?
    var address: String?
    
    //MARK: instance methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        transparentNav()
        manager.delegate = self
        manager.requestDetailedWeather()
        manager.requestCity()
    }
    
    override func viewDidAppear(animated: Bool) {
        let translate = CGAffineTransformMakeTranslation(0, 500)
        weatherImage.transform = translate
        tempLabel.transform = translate
        currentTime?.transform = translate
        cityLabel?.transform = translate
        UIView.animateWithDuration(2.5, delay: 0.2, usingSpringWithDamping: 0.8, initialSpringVelocity: 1, options: [], animations: {
            self.weatherImage.transform = CGAffineTransformIdentity
            }, completion: nil)
        UIView.animateWithDuration(2.5, delay: 0.4, usingSpringWithDamping: 0.8, initialSpringVelocity: 1, options: [], animations: {
            self.tempLabel.transform = CGAffineTransformIdentity
            self.currentTime?.transform = CGAffineTransformIdentity
            self.cityLabel?.transform = CGAffineTransformIdentity
            }, completion: nil)
    }
    
    func transparentNav() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: .Default)
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.navigationBar.translucent = true
        self.navigationController!.view.backgroundColor = UIColor.clearColor()
    }
    
    func presentTodayDetails() {
        let addChildView = self.storyboard?.instantiateViewControllerWithIdentifier("AddChild") as! AddChild
        self.addChildViewController(addChildView)
        addChildView.weatherResponse = self.weatherResponse
        if (self.address != nil) {
            addChildView.address = self.address
        } else {
            addChildView.address = self.cityLabel?.text
        }
        self.navigationController?.navigationBarHidden = true
        self.view.addSubview((addChildView.view)!)
        let viewsDict = ["child" : addChildView.view]
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[child]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[child]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDict))
        addChildView.didMoveToParentViewController(self)
        
    }
    
    func presentTableDetails() {
        let addChildView = self.storyboard?.instantiateViewControllerWithIdentifier("DailyTable") as! CustomTable
        self.addChildViewController(addChildView)
        addChildView.weatherResponse = self.weeklyWeather
        if (self.address != nil) {
            addChildView.address = self.address
        } else {
            addChildView.address = self.cityLabel?.text
        }
        self.view.addSubview((addChildView.view)!)
        
        self.navigationController?.navigationBarHidden = true
        let viewsDict = ["child" : addChildView.view]
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[child]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[child]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDict))
        addChildView.didMoveToParentViewController(self)
    }

    func presentDailyDetails() {
        let addChildView = self.storyboard?.instantiateViewControllerWithIdentifier("DailyDetails") as! DailyDetails
        self.addChildViewController(addChildView)
        addChildView.weatherResponse = weeklyWeather
        if (self.address != nil) {
            addChildView.address = self.address
        } else {
            addChildView.address = self.cityLabel?.text
        }
        self.view.addSubview((addChildView.view)!)
        
        self.navigationController?.navigationBarHidden = true
        let viewsDict = ["child" : addChildView.view]
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[child]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[child]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDict))
        addChildView.didMoveToParentViewController(self)
    }
    
    //MARK: action methods
    
    @IBAction func moreDetails(sender: AnyObject) {
        let actionSheetController: UIAlertController = UIAlertController(title: "More info: ", message: nil, preferredStyle: .ActionSheet)
        
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
        }
        actionSheetController.addAction(cancelAction)
        
        let todayDetails: UIAlertAction = UIAlertAction(title: "More details", style: .Default) { action -> Void in
            self.presentTodayDetails()
        }
        actionSheetController.addAction(todayDetails)
        
        let nextdays: UIAlertAction = UIAlertAction(title: "Next 3 days", style: .Default) { action -> Void in
            self.presentDailyDetails()
        }
        actionSheetController.addAction(nextdays)
        
        let tableDetails: UIAlertAction = UIAlertAction(title: "Next 7 days", style: .Default) { action -> Void in
            self.presentTableDetails()
        }
        actionSheetController.addAction(tableDetails)

        
        self.presentViewController(actionSheetController, animated: true, completion: nil)
    }
    
    @IBAction func presentSearchField() {
        //if searchTextField.
        dismissButton = UIButton(frame: CGRectMake(328, 127, 30, 30))
        dismissButton!.backgroundColor = UIColor.redColor()
        dismissButton!.layer.cornerRadius = 15
        dismissButton?.addTarget(self, action: #selector(buttonClicked), forControlEvents: .TouchUpInside)
        self.view.addSubview(dismissButton!)
        
        searchTextField = UITextField(frame: CGRectMake(60, 125, 250, 35))
        searchTextField!.placeholder = "Enter city, county"
        searchTextField!.font = UIFont.systemFontOfSize(15)
        searchTextField!.borderStyle = UITextBorderStyle.RoundedRect
        searchTextField!.autocorrectionType = UITextAutocorrectionType.No
        searchTextField!.keyboardType = UIKeyboardType.Default
        searchTextField!.clearButtonMode = UITextFieldViewMode.WhileEditing;
        searchTextField!.contentVerticalAlignment = UIControlContentVerticalAlignment.Center
        searchTextField!.delegate = self
        self.view.addSubview(searchTextField!)
        
        self.navigationController?.navigationBarHidden = true
    }
    
    func buttonClicked(sender: UIButton) {
        self.searchTextField?.removeFromSuperview()
        self.dismissButton?.removeFromSuperview()
        self.navigationController?.navigationBarHidden = false
    }
    
    @IBAction func doneButton(sender: UIBarButtonItem) {
        manager.requestDetailedWeather()
        manager.requestCity()
        self.view.reloadInputViews()
        self.viewDidAppear(true)
    }
    
    @IBAction func refreshButton(sender: UIBarButtonItem) {
        manager.requestDetailedWeather()
        manager.requestCity()
        self.view.reloadInputViews()
        self.viewDidAppear(true)
    }
    
    //MARK: delegate methods
    
    func doneGettingDetailedWeather(response: [Weather]) {
        weatherResponse = response
        WeatherInterpretation.changeBackground(weatherResponse[0], backgroundImage: backgroundImage, weatherImage: weatherImage, tempLabel: tempLabel, windSpeed: windSpeed)
        currentTime?.text = WeatherInterpretation.formatTime(weatherResponse[1].time)
        humidityLabel?.text = String(Int(Double(weatherResponse[0].humidity)*100)) + " %"
        summaryLabel?.text = weatherResponse[0].summary
    }
    
    func doneGettingWeeklyWeather(response: [WeeklyWeather]) {
        weeklyWeather = response
    }

    
    func textFieldShouldReturn(city: UITextField) -> Bool {
        city.resignFirstResponder()
        if (city.text! == "Bunicu dumitru"){
            address = "vatra dornei"
        } else if (city.text == "Bunica"){
            address = "roman"
        } else if (city.text == "Acasa") {
            address = "campulung moldovenesc"
        } else {
            address = city.text!
        }
        address = address!.capitalizedString
        cityLabel?.text = address
        manager.allRequests(address!)
        self.searchTextField?.removeFromSuperview()
        self.dismissButton?.removeFromSuperview()
        self.navigationController?.navigationBarHidden = false
        self.view.reloadInputViews()
        self.viewDidAppear(true)
        return true
    }
    
    func doneGettingCity(city: String) {
        address = city
        cityLabel?.text = city
    }
    
    //MARK: segue
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ShowMap" {
            self.searchTextField?.removeFromSuperview()
            self.dismissButton?.removeFromSuperview()
        }
        if segue.identifier == "Charts" {
            self.searchTextField?.removeFromSuperview()
            self.dismissButton?.removeFromSuperview()
            let destinationController = segue.destinationViewController as! BarChart
            destinationController.chartValues = weeklyWeather
        }
    }
}

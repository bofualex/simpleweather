
import UIKit
import MapKit

class LocationMap: UIViewController, MKMapViewDelegate, ManagerDelegate {
    
    @IBOutlet var mapView:MKMapView!
    @IBOutlet var addressLabel: UILabel?
    var address: String?
    let manager = Manager()
    
    //MARK: instance methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBarHidden = false
        manager.delegate = self
        manager.requestCity()
        showOnMap()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func showOnMap() {
        let annotation = MKPointAnnotation()
        annotation.title = GeoCode().currentLocation()
        let location = GeoCode().mapLocation()
        annotation.coordinate = location.coordinate
        self.mapView.showAnnotations([annotation], animated: true)
        self.mapView.selectAnnotation(annotation, animated: true)
        mapView.showsCompass = true
        mapView.showsScale = true
        mapView.showsTraffic = true
        mapView.delegate = self
    }
    
    // MARK: delegate methods
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "MyPin"
        
        if annotation.isKindOfClass(MKUserLocation) {
            return nil
        }
        var annotationView:MKPinAnnotationView? = mapView.dequeueReusableAnnotationViewWithIdentifier(identifier) as? MKPinAnnotationView
        
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView?.canShowCallout = true
        }
        annotationView?.pinTintColor = UIColor.orangeColor()
        
        return annotationView
    }
    
    func doneGettingCity(city: String) {
        address = city
        addressLabel?.text = address
        addressLabel?.reloadInputViews()
    }
}


import Foundation

class Weather: NSObject {
    
    
    let apparentTemperature: NSNumber
    let cloudCover: NSNumber
    let humidity: NSNumber
    let icon: String
    let summary: String
    let windSpeed: NSNumber
    let time: NSNumber
    
    init(apparentTemperature: NSNumber, cloudCover: NSNumber, humidity: NSNumber, icon: String, summary: String, windSpeed: NSNumber, time: NSNumber){
        self.apparentTemperature = apparentTemperature
        self.cloudCover = cloudCover
        self.humidity = humidity
        self.icon = icon
        self.summary = summary
        self.windSpeed = windSpeed
        self.time = time
    }
    
    init(dictionary: NSDictionary) {
        apparentTemperature = dictionary["apparentTemperature"] as! NSNumber
        cloudCover = dictionary["cloudCover"] as! NSNumber
        humidity = dictionary["humidity"] as! NSNumber
        icon = dictionary["icon"] as! String
        summary = dictionary["summary"] as! String
        windSpeed = dictionary["windSpeed"] as! NSNumber
        time = dictionary["time"] as! NSNumber
    }
    
}